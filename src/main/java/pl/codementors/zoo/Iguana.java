package pl.codementors.zoo;

public class Iguana extends Lizard implements Herbivorous{

    public void hiss (){
        System.out.println(getName());
        System.out.println("Iguana syczy");
    }


    @Override
    public void eat() {

        System.out.println(getName());
        System.out.println("Je liście");
    }

    @Override
    public void eatPlant() {
        System.out.println(getName());
        System.out.println("Iguana je rośliny");
    }
}
