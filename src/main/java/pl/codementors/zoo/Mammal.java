package pl.codementors.zoo;

abstract class Mammal extends Animal{


    @Override
    public void eat() {

    }

    private String furColor;

    public String getFurColor() {
        return furColor;
    }

    public void setFurColor(String furColor) {
        this.furColor = furColor;
    }
}
