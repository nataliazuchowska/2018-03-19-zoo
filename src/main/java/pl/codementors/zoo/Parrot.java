package pl.codementors.zoo;

public class Parrot extends Bird implements Herbivorous{


    public void screech(){

        System.out.println(getName());
        System.out.println("Papuga skrzeczy");
    }

    @Override
    public void eat() {

        System.out.println(getName());
        System.out.println("Je ziarno");
    }

    @Override
    public void eatPlant() {

        System.out.println(getName());
        System.out.println("Papuga je rośliny");
    }

}

