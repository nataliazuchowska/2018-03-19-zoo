package pl.codementors.zoo;
abstract class Lizard extends Animal{


    @Override
    public void eat() {

    }

    private String scaleColor;

    public String getScaleColor() {
        return scaleColor;
    }

    public void setScaleColor(String scaleColor) {
        this.scaleColor = scaleColor;
    }
}
