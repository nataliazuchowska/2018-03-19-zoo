package pl.codementors.zoo;

public class Wolf extends Mammal implements Carnivorous {

    public void howl(){ // utworzenie wlasnej metody wycia dla wilkow
        System.out.println(getName());
        System.out.println("Wilk wyje");
    }


    @Override
    public void eat() { // nadpisanie metody eat z klasy Animal
        System.out.println(getName());
        System.out.println("Je mięso");
    }

    @Override
    public void eatMeat() { // nadpisanie interfejsu dla miesozernych
        System.out.println(getName());
        System.out.println("Wilk je mięso");
    }
}

