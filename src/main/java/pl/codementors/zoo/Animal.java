package pl.codementors.zoo;

import java.io.Serializable;

public abstract class Animal implements Serializable { // zaimplementowanie serializacji jest potrzebne do utworzenia pliku binarnego z danej klasu

    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public abstract void eat();
}
