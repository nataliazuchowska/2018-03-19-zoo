package pl.codementors.zoo;

abstract class Bird extends Animal {


    @Override
    public void eat() {
        System.out.println("This is a bird");
    }

    private String featherColor;

    public String getFeatherColor() {
        return featherColor;
    }

    public void setFeatherColor(String featherColor) {
        this.featherColor = featherColor;
    }
}
