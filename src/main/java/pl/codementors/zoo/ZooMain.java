package pl.codementors.zoo;

import java.io.*;

/**
 * Hello world!
 *
 */
public class ZooMain {
    public static void main(String[] args) {
        System.out.println("Hello to the zoo");


        //stworzona została tablica dla 10 zwierzątek
        Animal[] animals = new Animal[10];

        // każdemu zwierzątku przypisuje typ oraz imie
        animals[0] = new Parrot();
        animals[0].setName("Zuza");
        ((Parrot) animals[0]).setFeatherColor("Czerwony");

        animals[1] = new Wolf();
        animals[1].setName("Michał");
        ((Wolf) animals[1]).setFurColor("Szary");

        animals[2] = new Iguana();
        animals[2].setName("Kasia");
        ((Iguana) animals[2]).setScaleColor("Zielony");

        animals[3] = new Wolf();
        animals[3].setName("Maciej");
        ((Wolf) animals[3]).setFurColor("Popielaty");

        animals[4] = new Iguana();
        animals[4].setName("Krysia");
        ((Iguana) animals[4]).setScaleColor("Kolorowy");

        animals[5] = new Parrot();
        animals[5].setName("Zbyszek");
        ((Bird) animals[5]).setFeatherColor("Niebieski");

        animals[6] = new Parrot();
        animals[6].setName("Zyta");
        ((Parrot) animals[6]).setFeatherColor("Pomaranczowy");

        animals[7] = new Wolf();
        animals[7].setName("Mikołaj");
        ((Wolf) animals[7]).setFurColor("Czarny");

        animals[8] = new Wolf();
        animals[8].setName("Marcin");
        ((Wolf) animals[8]).setFurColor("Biały");

        animals[9] = new Iguana();
        animals[9].setName("Krzysiek");
        ((Iguana) animals[9]).setScaleColor("Fioletowy");

        //wywołanie metody print (bez tego ona się nie uruchomi) metoda pokazuje nam na ekran zwierzątka
        print(animals);

        //wywołanie metody feed (bez tego ona sie nie uruchomi) metoda karmi zwierzeta
        feed(animals);

        // wywołanie metody zapisz do pliku (bez tego plik się nie utowrzy)
        saveToFile(animals);

        // wywołanie metody wycia na wszystkich wilkach
        howl(animals);

        // wywołanie metody syczenia na iguanach
        hiss(animals);

        // wywołanie metody skrzeczenia na wszystkich papugach
        screech(animals);

        // nakarmienie wszystkich zwierzatek miesozernych
        feedWithMeat(animals);

        //nakarmienie wszystkich zwierzatek roslinozernych
        feedWithPlant(animals);

        //wywołanie utworzenia tablicy z czytanych elementów ze ścieżki
        Animal[] tablicaZCzytania = readFromFile("/tmp/fileAnimals.txt");

        printColors(animals);

        saveToBinaryFile(animals, "/tmp/fileAnimalsBinary.txt");

        Animal[] tablicaZPlikuBinarnego = readFromBinaryFile("/tmp/fileAnimalsBinary.txt");

        System.out.println(tablicaZPlikuBinarnego[1].getName());


    }

    static void print(Animal[] anim) {

        // z każdego zwierzątka z tablicy pobieram jego lokalizację (klasę) oraz imię
        for (int i = 0; i < anim.length; i++) {
            System.out.println(anim[i].getClass());
            System.out.println(anim[i].getName());
        }
    }

    static void feed(Animal[] animals) {

        /*dla każdego zwierzątka w tablicy wywołuje eat które jest zaimplementowane
        w każdej klasie dla poszczególnego zwierzątka
         */
        for (int i = 0; i < animals.length; i++) {
            animals[i].eat();

        }
    }

    static void saveToFile(Animal[] animals) { // metoda zapisujaca tablice animalow do pliku

        //tworze plik tekstowy fileAnimals na podanej ścieżce tmp
        File fileAnimals = new File("/tmp/fileAnimals.txt");

        // utworzenie file writera aby moc zapisac w pliku
        FileWriter fw = null;
        //łapanie wyjątku
        try {
            fw = new FileWriter(fileAnimals);
            //pętla do iterowania po wszystkich animalsach w celu napisania imienia i klasy
            for (int i = 0; i < animals.length; i++) {

                fw.write(animals[i].getName());
                fw.write("\n"); //znak nowej lini w pliku tekstowym
                fw.write(animals[i].getClass().toString());
                fw.write("\n");
            }
            fileAnimals.setExecutable(false);
            fw.close(); //zamykanie filewritera
        } catch (IOException e) {
            System.err.println(e.getMessage()); // łapanie wyjątku jeśli coś pojdzie nie tak poda nam wiadomość
        } finally { // blok finally wykona się bez względu na wszystko
            if (fw != null) { // jeżeli fw nie jest puste niech sprobuje zamknac fw
                try {
                    fw.close();
                } catch (IOException e) { // łapiemy wyjątek gdyby coś było nie tak
                    System.err.println(e.getMessage());
                }
            }
        }
    }

    static void howl(Animal[] animals) { //metoda wywołania wycia na zwierzetach

        for (int i = 0; i < animals.length; i++) {

            if (animals[i] instanceof Wolf) { // sprawdzanie czy animal jest obiektem nalezacym do wilka
                ((Wolf) animals[i]).howl(); //polimorfizm uwazaj!
            }
        }
    }

    // analogiczne pozostale klasy
    static void hiss(Animal[] animals) { // metoda wywolujaca syczenie na zwierzetach

        for (int i = 0; i < animals.length; i++) {

            if (animals[i] instanceof Iguana) { // sprawdzanie czy dany animal jest iguana
                ((Iguana) animals[i]).hiss(); // polimorfizm oraz wywulanie metody
            }
        }
    }

    static void screech(Animal[] animals) { // metoda wywolujaca skrzeczenie na zwierzetach

        for (int i = 0; i < animals.length; i++) {

            if (animals[i] instanceof Parrot) { // sprawdzanie czy dany animal jest parrotem (metoda skrzeczenia tylko dla parrotow)
                ((Parrot) animals[i]).screech(); // polimorfizm i wywolanie skrzeczenia
            }
        }
    }

    static void feedWithMeat(Animal[] animals) { // metoda karmiaca zwierzaki miesem

        for (int i = 0; i < animals.length; i++) {

            if (animals[i] instanceof Wolf) { // sprawdzanie czy dany animal jest wilkiem
                ((Wolf) animals[i]).eatMeat(); // polimorfizm oraz wywolanie metody nakarmienia miesem

            }
        }
    }

    static void feedWithPlant(Animal[] animals) { // metoda karmiaca roslinami zwierzeta

        for (int i = 0; i < animals.length; i++) {

            if (animals[i] instanceof Parrot) { // sprawdzenie czy zwierzak jest papuga poniewaz papuga ma zaimplementowana tą metode
                ((Parrot) animals[i]).eatPlant(); // polimorfizm oraz nakarmienie papug

            } else if (animals[i] instanceof Iguana) { // sprawdzenie czy dany zwierzak jest iguana
                ((Iguana) animals[i]).eatPlant(); // polimorfizm oraz nakarmienie iguan
            }
        }
    }

    static Animal[] readFromFile(String pathToFile) { // metoda czytajaca z pliku zwracajaca tablice zwierzat

        File file = new File(pathToFile); // utworzenie nowego pliku ze sciezka do pliku

        Animal[] tablicaZwierzat = new Animal[20]; // utworzenie tablicy zwierzat ze spodziewanym rozmiarem jaki otrzymamy z czytania z pluku. 20 poniewaz imie zwierzatka + jego klasa
        int i = 0;
        try {
            System.out.println("Czytamy plik" + pathToFile); // czytamy plik z danej ściezki
            FileReader fr = new FileReader(file); // utworzenie FileReadera do czytania pliku
            BufferedReader bf = new BufferedReader(fr); // rozszerzenie FileReadera do BufferedReadera

            String s = bf.readLine(); // przypisanie stringa do przeczytania lini z pliku
            String imie = null;

            while (s != null) { // jezeli dana linia w pliku nie jest pusta to przeczytaj:
                if (s.contains("Parrot")) { // jezeli linia tekstu w pliku zwiera "Parrot"
                    tablicaZwierzat[i] = new Parrot(); // to utworz w tablicy nowego Parrota
                    tablicaZwierzat[i].setName(imie); // oraz ustaw mu imie
                    i++;
                } else if (s.contains("Wolf")) {
                    tablicaZwierzat[i] = new Wolf();
                    tablicaZwierzat[i].setName(imie);
                    i++;
                } else if (s.contains("Iguana")) {
                    tablicaZwierzat[i] = new Iguana();
                    tablicaZwierzat[i].setName(imie);
                    i++;
                } else {
                    imie = s;
                }
                s = bf.readLine();
            }

        } catch (Exception e) { // łapanie wyjatku
            e.getMessage(); // podanie wiadomosci go jet nie tak

        } finally {

        }
        return tablicaZwierzat; // zwrocenie tablicy zwierzat

    }

    static void printColors(Animal[] animals) { // wywolanie metody podajaca kolor siersci zwierzat


        for (int i = 0; i < animals.length; i++) {

            animals[i].getName(); // podanie imienia zwierzatka

            if (animals[i] instanceof Wolf) {
                System.out.println(animals[i].getName());
                System.out.println(((Wolf) animals[i]).getFurColor());

            } else if (animals[i] instanceof Iguana) {
                System.out.println(animals[i].getName());
                System.out.println(((Iguana) animals[i]).getScaleColor());

            } else if (animals[i] instanceof Parrot) {
                System.out.println(animals[i].getName());
                System.out.println(((Parrot) animals[i]).getFeatherColor());
            }


        }
    }

    static void saveToBinaryFile(Animal[] animals, String path) { // zapis do pliku bibarnego tablicy zwierzatk , podanie sciezki
        // tu wymagana jest serializacja. Zaimplementowanie serializacji w klasie Animal bo to je pracujemy (zwierze + imie)
        FileOutputStream fos = null;
        File plikBinarny = new File(path); // utworzenie plikuBinarnego do podanej sciezki

        try {
            fos = new FileOutputStream(plikBinarny); // strumien wyjsciowy na pliku binarnym
            ObjectOutputStream oos = new ObjectOutputStream(fos); //utworzenie obiektowego strumienia do czytania
            oos.writeObject(animals); // napisania wszsystkich animalow
            oos.flush(); // wymuszenie zamkniecia
            oos.close(); // zamkniecie
        } catch (FileNotFoundException ex) {
            System.err.println(ex.getMessage());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static Animal[] readFromBinaryFile(String path) { // czytanie z pliku binarnego zwracajacy tablice animalow

        Animal[] tablicaZwierzatZOdczytu = new Animal[10]; // utworzenie nowej tablicy z odczytu
        FileInputStream fis = null;
        File plik = new File(path); // utworzenie pliku do ściezki

        try {
            fis = new FileInputStream(plik); // przypisanie FileInputStream do pliku
            ObjectInputStream ois = new ObjectInputStream(fis); // przypisanie ObjectImputStream do fis'a

            tablicaZwierzatZOdczytu = (Animal[]) (ois.readObject()); // zczytanie tablicy do tablicy zwierzat
            System.out.println(tablicaZwierzatZOdczytu); // wypisanie tablicy zwierzat
            ois.close(); // zamkniecie strumienia
            fis.close();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        } finally {

        }
        return tablicaZwierzatZOdczytu; // zwrocenie tablicy z Odczytu
    }


}